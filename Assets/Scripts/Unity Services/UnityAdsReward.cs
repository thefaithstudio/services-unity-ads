using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Advertisements;
 
public class UnityAdsReward : MonoBehaviour {
 
	#region PUBLIC VARIABLES

	public static UnityAdsReward instance;

	[Header("RewardVideoAds Property")]
	public 	bool 		enableRewardVideoAds;
	public 	bool rewardedVideoAdsSkipCallback;
	public	bool rewardedVideoAdsFailedCallBack;

	public UnityEvent	OnRewardVideoAdsFinished;
	public UnityEvent	OnRewardVideoAdsSkipped;
	public UnityEvent	OnRewardVideoAdsFailed;

	[Header("VideoAds Property")]
	public 	bool 		enableVideoAds;
	public	bool videoAdsSkipCallback;
	public 	bool videoAdsFailedCallback;

	public UnityEvent	OnVideoAdsFinished;
	public UnityEvent	OnVideoAdsSkipped;
	public UnityEvent	OnVideoAdsFailed;

	//--------------------------------
	public bool 		unityTestMode;
	public bool 		testModeGUI;

	public 	string 		gameID;
	public 	string 		gameID_Android;
	public 	string 		gameID_iOS;

	#endregion

	#region PRIVATE VARIABLES

	private	bool mCustomCall;

	private UnityAction OnAdsFinished;
	private UnityAction OnAdsSkipped;
	private UnityAction OnAdsFailed;

	#endregion

    void Awake(){

		mPreProcess ();
    }
 
	#region Pre-Process/Process/Post-Process

    void OnGUI(){
 
        if (testModeGUI) {
 
            float width = 0; 
            float height = 0;
 
            if (Screen.width > Screen.height) {
                //Landscape
                width = Screen.width/3;
                height = Screen.height / 16;
 
            } else {
                //Portrait
                width = Screen.width / 2;
                height = Screen.height / 12;
            }
 
            if (Advertisement.isSupported) {
             
                GUI.TextField (new Rect (0, 0, width, height),
                    "Unity Ads Supported");
            }
 
			if (GUI.Button    (new Rect (0,height, width, height), 
				"Show Ads")) {

				ShowAds ();
			}

            if (Advertisement.IsReady ("rewardedVideo")) {
             
                GUI.TextField (new Rect (width, 0, width, height),
                    "rewardVideo (Ready)");
            } else {
 
				GUI.TextField (new Rect (width, 0, width, height),
                    "rewardVideo\n(NOT! Ready)");
            }
 
            if (Advertisement.IsReady ("video")) {
 
				GUI.TextField (new Rect (width, height, width, height), 
                    "video (Ready)");
            } else {
 
				GUI.TextField (new Rect (width, height, width, height), 
                    "video (NOT! Ready)");
            }

			if (GUI.Button    (new Rect (0,height*2, width, height), 
				"Show Ads\n(RwVAds)")) {

				ShowRewardVideoAds ();
			}

			if (GUI.Button    (new Rect (width,height*2, width, height), 
				"Show Ads\n(VAds)")) {

				ShowVideoAds ();
			}
        }   
    }
 
	private void mPreProcess(){

		mCustomCall = false;;

		mResetCustomAdsCallBack ();

		if (instance == null) {

			instance = this;
			DontDestroyOnLoad (instance.gameObject);
		} else if(instance != this){

			Destroy (this.gameObject);
		}

		#if UNITY_ANDROID

		gameID = gameID_Android;

		#elif UNITY_IOS

		gameID = gameID_iOS;

		#endif

		StartCoroutine (InitializedAds ());
	}

    private IEnumerator InitializedAds(){
 
        if (Advertisement.isSupported) {
 
            Advertisement.Initialize (gameID, unityTestMode);
        }
 
        while (!Advertisement.isInitialized || !Advertisement.IsReady ()) {
 
            yield return new WaitForSeconds (0.5f);
        }
    }
 
	private void mResetCustomAdsCallBack(){

		OnAdsFinished 	= null;
		OnAdsSkipped 	= null;
		OnAdsFailed 	= null;
	}
 
	#endregion

    //-----------------------------------------------------------------------------
 	
	#region RewardedVide Ads

	public bool IsRewardedVideoAdsReady(){

		if (Advertisement.IsReady ("rewardedVideo"))
			return true;

		return false;
	}

    public void ShowRewardVideoAds(){
 
        if (Advertisement.IsReady ("rewardedVideo")) {
 
            Advertisement.Show ("rewardedVideo", new ShowOptions () {
 
                resultCallback = HandleRewardAdResult
            });
        }
    }
 
	public void ShowRewardedVideoAdsWithEvent(UnityAction OnAdsFinished){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		mCustomCall = true;

		ShowRewardVideoAds ();
	}

	public void ShowRewardedVideoAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsFailed 	= OnAdsFailed;
		mCustomCall = true;

		ShowRewardVideoAds ();
	}

	public void ShowRewardedVideoAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsSkipped,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsSkipped 	= OnAdsSkipped;
		this.OnAdsFailed 	= OnAdsFailed;
		mCustomCall = true;

		ShowRewardVideoAds ();
	}

    private void HandleRewardAdResult(ShowResult result){
 
        switch (result) {
 
		case ShowResult.Failed:
			if (mCustomCall) {

				OnAdsFailed.Invoke ();
				mCustomCall = false;
			} else {

				OnRewardVideoAdsFailed.Invoke ();
			}
            //Debug.Log ("Failed To Load The RewardAds");
            break;
		case ShowResult.Skipped:
			if (mCustomCall) {

				OnAdsSkipped.Invoke ();
				mCustomCall = false;
			} else {

				OnRewardVideoAdsSkipped.Invoke ();
			} 
			//Debug.Log ("Skip RewardAds");
            break;
		case ShowResult.Finished:
			if (mCustomCall) {

				OnAdsFinished.Invoke ();
				mCustomCall = false;
			} else {

				OnRewardVideoAdsFinished.Invoke ();
			}
            //Debug.Log ("Finished RewardAds");
            break;
        }
    }
 
	#endregion

    //-----------------------------------------------------------------------------
 
	#region VideoAds

	public bool IsVideoAdsReady(){

		if (Advertisement.IsReady ("rewardedVideo"))
			return true;

		return false;
	}

    public void ShowVideoAds(){
 
        if(Advertisement.IsReady("video")){
 
            Advertisement.Show ("video", new ShowOptions () {
 
                resultCallback = HandleVideoAdResult
            });
        }
    }
 
	public void ShowVideoAdsWithEvent(UnityAction OnAdsFinished){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		mCustomCall = true;

		ShowVideoAds ();
	}

	public void ShowVideoAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsFailed 	= OnAdsFailed;
		mCustomCall = true;

		ShowVideoAds ();
	}

	public void ShowVideoAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsSkipped,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsSkipped 	= OnAdsSkipped;
		this.OnAdsFailed 	= OnAdsFailed;
		mCustomCall = true;

		ShowVideoAds ();
	}

    private void HandleVideoAdResult(ShowResult result){
 
        switch (result) {
 
        case ShowResult.Failed:
			if (mCustomCall) {

				OnAdsFailed.Invoke ();
			} else {

				OnVideoAdsFailed.Invoke ();
				mCustomCall = false;
			}
			//Debug.Log ("Failed To Load The VideoAds");
            break;
		case ShowResult.Skipped:
			if (mCustomCall) {

				OnAdsSkipped.Invoke ();
			} else {

				OnVideoAdsSkipped.Invoke ();
				mCustomCall = false;
			}
            //Debug.Log ("Skip VideoAds");
            break;
        case ShowResult.Finished:
			if (mCustomCall) {

				OnAdsFinished.Invoke ();
			} else {

				OnVideoAdsFinished.Invoke ();
				mCustomCall = false;
			}
            //Debug.Log ("Finished VideoAds");
            break;
        }
    }

	#endregion

	//-----------------------------------------------------------------------------

	#region ShowAds (With Priority of Rewarded Video Ads -> Video Ads)

	public bool IsAdsReady(){

		if (Advertisement.IsReady ())
			return true;

		return false;
	}

	public void ShowAds(){

		if (Advertisement.IsReady ("rewardedVideo")) {

			Advertisement.Show ("rewardedVideo", new ShowOptions () {

				resultCallback = HandleRewardAdResult
			});
		} else if (Advertisement.IsReady ("video")) {

			Advertisement.Show ("video", new ShowOptions () {

				resultCallback = HandleVideoAdResult
			});
		}
	}

	public void ShowAdsWithEvent(UnityAction OnAdsFinished){

		mResetCustomAdsCallBack ();
		this.OnAdsFinished = OnAdsFinished;
		ShowAds ();
	}

	public void ShowAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();
		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsFailed 	= OnAdsFailed;
		ShowAds ();
	}

	public void ShowAdsWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsSkipped,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();
		this.OnAdsFinished 	= OnAdsFinished;
		this.OnAdsSkipped 	= OnAdsSkipped;
		this.OnAdsFailed 	= OnAdsFailed;
		ShowAds ();
	}

	#endregion
}