﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UnityAdsReward))]
public class UnityAdsRewardEditor : Editor {

	private UnityAdsReward mUARReference;

	private string  Is_Android_Platform_Enabled	=	"Is_Android_Platform_Enabled";
	private string  Is_iOS_Platform_Enabled		=	"Is_iOS_Platform_Enabled";

	private string 	gameID_AndroidLocal;

	private void mPreProcess(){
	
		mUARReference = (UnityAdsReward)target;

	}

	private void OnEnabled(){

		mPreProcess ();
	}

	public override void OnInspectorGUI(){

		mPreProcess ();

		serializedObject.Update ();

		TestModeGUI ();

		PlatformSelectionGUI ();

		AdsTypeSelectionGUI ();

		serializedObject.ApplyModifiedProperties ();

	}

	private void TestModeGUI(){

		#region Test Mode
		EditorGUILayout.Space ();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.HelpBox(
			"Allow you to test the unity ads without seeing actual ads." +
			"But you need to configure this on unity dashboard and puting " +
			"'gameID' under platform selection even if you want to test it",
			MessageType.Warning);
		EditorGUILayout.PropertyField(
			serializedObject.FindProperty("unityTestMode"));
		EditorGUILayout.EndHorizontal();

		GUILayout.Box("",GUILayout.ExpandWidth(true), GUILayout.Height(2.5f));
		EditorGUILayout.Space ();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.HelpBox(
			"Allow you to test unity ads services with GUI buttons to test on" +
			"physical device with callback",
			MessageType.Info);
		EditorGUILayout.PropertyField(
			serializedObject.FindProperty("testModeGUI"));
		EditorGUILayout.EndHorizontal();
		#endregion
	}

	private void PlatformSelectionGUI(){

		#region Platform Selection
		GUILayout.Box("",GUILayout.ExpandWidth(true), GUILayout.Height(2.5f));
		EditorGUILayout.Space ();
		EditorGUILayout.LabelField ("Select Platforms");

		EditorGUI.indentLevel++;
		EditorGUILayout.BeginVertical();

		// Android
		EditorGUILayout.BeginHorizontal();
		if(PlayerPrefs.GetInt(Is_Android_Platform_Enabled) == 1){

			if(GUILayout.Button("Disable : Android")){

				PlayerPrefs.SetInt(Is_Android_Platform_Enabled,0);
			}
				
			EditorGUILayout.PropertyField(
				serializedObject.FindProperty("gameID_Android"));

		}else{

			if(GUILayout.Button("Enable Android")){

				PlayerPrefs.SetInt(Is_Android_Platform_Enabled,1);
			}

			serializedObject.FindProperty("gameID_Android").stringValue = "";
		}
		EditorGUILayout.EndHorizontal();
		//-------------

		// iOS
		EditorGUILayout.BeginHorizontal();
		if(PlayerPrefs.GetInt(Is_iOS_Platform_Enabled) == 1){

			if(GUILayout.Button("Disable :      iOS")){

				PlayerPrefs.SetInt(Is_iOS_Platform_Enabled,0);
			}

			EditorGUILayout.PropertyField(
				serializedObject.FindProperty("gameID_iOS"));
		}else{

			if(GUILayout.Button("Enable iOS")){

				PlayerPrefs.SetInt(Is_iOS_Platform_Enabled,1);
			}

			serializedObject.FindProperty("gameID_iOS").stringValue = "";
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.EndVertical();
		EditorGUI.indentLevel--;
		#endregion
	}

	private void AdsTypeSelectionGUI(){

		#region Ads Type Selection
		EditorGUILayout.Space ();
		EditorGUILayout.Space ();
		GUILayout.Box("",GUILayout.ExpandWidth(true), GUILayout.Height(2.5f));
		OnRewardedVideoAdsGUI();
		OnVideoAdsGUI();
		#endregion
	}

	private void OnRewardedVideoAdsGUI(){

		EditorGUILayout.BeginVertical();

		EditorGUILayout.BeginHorizontal();

		mUARReference.enableRewardVideoAds = 
			EditorGUILayout.Foldout(
				mUARReference.enableRewardVideoAds,
				mUARReference.enableRewardVideoAds ? "RewardVideoAds(Enabled)" : "RewardVideoAds(Disabled)");
		
		if (mUARReference.enableRewardVideoAds) {

			EditorGUI.indentLevel += 2;

			EditorGUILayout.PropertyField (
				serializedObject.FindProperty (
					"OnRewardVideoAdsFinished"),
				true
			);
			EditorGUILayout.EndHorizontal ();


			EditorGUILayout.Space ();
			EditorGUILayout.Space ();
			EditorGUILayout.BeginHorizontal ();
			mUARReference.rewardedVideoAdsSkipCallback = EditorGUILayout.Toggle (
				"Skip Callback",
				mUARReference.rewardedVideoAdsSkipCallback);

			if (mUARReference.rewardedVideoAdsSkipCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnRewardVideoAdsSkipped")
				);
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.Space ();
			EditorGUILayout.Space ();
			EditorGUILayout.BeginHorizontal ();
			mUARReference.rewardedVideoAdsFailedCallBack = EditorGUILayout.Toggle (
				"Failed Callback",
				mUARReference.rewardedVideoAdsFailedCallBack);

			if (mUARReference.rewardedVideoAdsFailedCallBack) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnRewardVideoAdsFailed")
				);
			}
			EditorGUILayout.EndHorizontal ();
			EditorGUI.indentLevel -= 2;
		}

		EditorGUILayout.EndVertical();
	}

	private void OnVideoAdsGUI(){

		EditorGUILayout.Space ();
		EditorGUILayout.Space ();
		EditorGUILayout.BeginVertical();

		EditorGUILayout.BeginHorizontal();
		mUARReference.enableVideoAds = 
			EditorGUILayout.Foldout(
				mUARReference.enableVideoAds,
				mUARReference.enableVideoAds ? "VideoAds(Enable)" : "VideoAds(Disabled)");

		if (mUARReference.enableVideoAds) {

			EditorGUI.indentLevel += 2;

			EditorGUILayout.PropertyField (
				serializedObject.FindProperty ("OnVideoAdsFinished")
			);
			EditorGUILayout.EndHorizontal ();


			EditorGUILayout.Space ();
			EditorGUILayout.Space ();
			EditorGUILayout.BeginHorizontal ();
			mUARReference.videoAdsSkipCallback = EditorGUILayout.Toggle (
				"Skip Callback",
				mUARReference.videoAdsSkipCallback);

			if (mUARReference.videoAdsSkipCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnVideoAdsSkipped")
				);
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.Space ();
			EditorGUILayout.Space ();
			EditorGUILayout.BeginHorizontal ();
			mUARReference.videoAdsFailedCallback = EditorGUILayout.Toggle (
				"Failed Callback",
				mUARReference.videoAdsFailedCallback);

			if (mUARReference.videoAdsFailedCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnVideoAdsFailed")
				);
			}
			EditorGUILayout.EndHorizontal ();
			EditorGUI.indentLevel -= 2;
		} 

		EditorGUILayout.EndVertical();
	}

}
