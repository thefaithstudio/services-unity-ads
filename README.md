
# Documentation
You can find the full **Documentation** on the following [link](https://bitbucket.org/PGS_ART/service-unity-ads/src/2fbf7af415ff974dad61f3e2585d33bbc63bb5e8/Assets/HelpTool/Documentation.docx?at=master&fileviewer=file-view-default)

# Editor Settings

- **Unity Test Mode:** Allow you to test the unity ads without seeing actual ads. But you need to configure this on unity dashboard and putting **'gameID'** under platform selection even if you want to test it.

- **Test Mode GUI**** :** Allow you to test the unity ads services with GUI buttons to test on. Physical device with callback

- **Select Platforms:** You need to enable the platform by simply clicking on the &quot; **"Enable Android/Enable iOS"** in order to run the unity ads on different physical device of different platform. You also need to provide the 'gameID' of the specific platform to the 'gameID' field ( **Game ID\_Android/Game ID\_iOS** ) which can be found on [unity ads dashboard](https://unityads.unity3d.com/admin/). You can also disabled the platform by simply clicking on the &quot; **Disable : Android/Disable : iOS**&quot; button.

- **Enable Ads:** By simply clicking on the "RewardVideoAds(Disabled)" (Toggle) button, you will be able to enable the "RewardVideoAds" on unity platform. The "OnRewardVideoAdsFinished" is triggered when user successfully shown the whole ads. So any callback for reward can be put here.

- **Disable Ads:** You can also disable "RewardVideoAds" and "VideoAds" by simply clicking on the "toggle" section.

# Custom Function

Overview: as the following class is a singleton, only one object will stay in all scene, the available function can be simply call by &quot; **UnityAdsReward.Instance.**** YourFunction**&quot;.

**RewardVIdeoAds:**

- **IsRewardedVideoAdsReady()**: return true, if the "RewardVideoAds" ready. Else false.
- **ShowRewardVideoAds()**: If the "RewardVideoAds" is ready, it will show the ad.
- **ShowRewardVideoAdsWithEvent(UnityAction OnAdsFinished)**: Show ad when ready, and if the ad is successfully finished, it will trigger your custom call back for finishing the ads.
- **ShowRewardVideoAdsWithEvent(UnityAction OnAdsFinished,OnAdsFailed)**: Show ad when ready, and if the ad is successfully finished or failed to load, it will trigger your custom call back for finishing &amp; failing to load the ads.
- **ShowRewardVideoAdsWithEvent(UnityAction OnAdsFinished,OnAdsSkipped,OnAdsFailed)**: Show ad when ready, and if the ad is successfully finished,skipped or failed to load, it will trigger your custom call back for finishing, skipping failing to load the ads.

**VIdeoAds:**

- **IsVideoAdsReady()**: return true, if the "VideoAds" ready. Else false.
- **ShowVideoAds()**: If the "VideoAds" is ready, it will show the ad.
- **ShowVideoAdsWithEvent(UnityAction OnAdsFinished)**: Show ad when ready, and if the ad is successfully finished, it will trigger your custom call back for finishing the ads.
- **ShowVideoAdsWithEvent(UnityAction OnAdsFinished,OnAdsFailed)**: Show ad when ready, and if the ad is successfully finished or failed to load, it will trigger your custom call back for finishing &amp; failing to load the ads.
- **ShowVideoAdsWithEvent(UnityAction OnAdsFinished,OnAdsSkipped,OnAdsFailed)**: Show ad when ready, and if the ad is successfully finished,skipped or failed to load, it will trigger your custom call back for finishing,skipping &amp; failing to load the ads.

